""" Задание №2.
Переделать Задание №1 с созданием и использованием собственное исключение
WhitespaceError с атрибутами:
    position - позиция в строке
    symbol - какой именно непечатный символ
Функция main() демонстрирует работу вашей функции, должна красиво показывать
что именно вызвало исключение.
"""
from string import whitespace


class WhitespaceError(Exception):
    def __init__(self, position, symbol):
        self.position = position
        self.symbol = symbol


def string_processing(text, *args, **kwargs):
    # блок вашего кода, который преобразовывает строку к новому виду,
    # которое сохроняете в переменной result
    """
    Там, где у вас вызывается исключение,
    необходимо сохранить позицию и ошибочный символ в атрибуты исключения
    следующим блоком кода
    вызов_исключения WhitespaceError(ваща_переменна_позиции, ваша_переменная_символа)
    """
    for t in whitespace:
        if text.find(t) >= 0: raise WhitespaceError(text.find(t), t.__repr__())
    return text.upper()


def main():
    """
    Вызываете свою функцию с тестовыми данными и красиво сообщаете о том, что произошло
    """
    try:
        print(string_processing('ex\tample'))
    except WhitespaceError as e:
        print('Ошибка:', e)
    except Exception:
        print('some exception')



if __name__ == "__main__":
    main()
