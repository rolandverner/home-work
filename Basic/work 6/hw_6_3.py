""" Задание №3.
Создать функцию, которая анализирует параметры и если хотя-бы один из них
другого типа, то вызывает исключение (на выбор - одно из стандартных или своё
собственное по примеру Задания №2).
В комментарии обосновать свой выбор.

В функции main() независиимо от того было исключение или не было,
всё-равно надо сообщить с какими аргументами вы вызывали свою функцию и
какого типа был каждый аргумент.
"""


class TypeeError(Exception):
    def __init__(self, string, tp):
        self.string = string
        self.tp = tp

def check_params_type(*args, **kwargs):
    arch = {}
    for text in args:
        print(text, type(text), sep=", ")
        arch.setdefault(type(text))
        if len(arch) >= 2: raise TypeeError(text, type(text))
    return arch.keys()

def main():
    data = ['asd', 'adfadsf', 'asdfasdf', 123]

    try:
        print(check_params_type(*data))
    except TypeeError as e:
        print('Ошибка:', e)
    except Exception:
        print('some exception')


if __name__ == "__main__":
    main()
