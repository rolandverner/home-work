""" Задание №1.
функция что-то делает со строкой и для непечатных символов (string.whitespace)
вызывает исключение ValueError
"""
from string import whitespace

def string_processing(text, *args, **kwargs):
    # блок вашего кода, который преобразовывает строку к новому виду,
    # которое сохроняете в переменной result

    for t in whitespace:
        if text.find(t) >= 0: raise ValueError('В строке есть символы whitespace', t.encode())
    return text.upper()


if __name__ == "__main__":
    # Здесь можно поместить свой код, который проверяет правильную работу
    # вашей функции
    try:
        print(string_processing('asdfasd\tfasasdfasdfasdf'))
    except ValueError as e:
        print('Ошибка:', e)
