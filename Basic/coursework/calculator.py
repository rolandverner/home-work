number_first = float(input('Введите первое число: '))
operator = input('Введи оператор\n+\n-\n*\n/\n')
number_second = float(input('Введите второе число: '))

if operator == '+':
    # Плюс
    print('{} + {} = {}'.format(number_first, number_second, number_first + number_second))
elif operator == '-':
    # Минус
    print('{} - {} = {}'.format(number_first, number_second, number_first - number_second))
elif operator == '*':
    # Множение
    print('{} * {} = {}'.format(number_first, number_second, number_first * number_second))
elif operator == '/':
    # Деление
    # Проблема с 0 / 0 надо ловить
    if number_second != 0:
        print('{} / {} = {}'.format(number_first, number_second, number_first / number_second))
else:
    print('введи то что написано выше')
