from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QGridLayout, QLayout, QLineEdit, QSizePolicy, QToolButton, QWidget)


class Button(QToolButton):
    def __init__(self, text, parent=None):
        super(Button, self).__init__(parent)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.setText(text)

    def sizeHint(self):
        size = super(Button, self).sizeHint()
        size.setHeight(size.height() + 10)
        size.setWidth(max(size.width(), size.height()))
        return size


class Calculator(QWidget):

    def __init__(self, parent=None):
        super(Calculator, self).__init__(parent)

        self.AdditiveOperator = ''
        self.MultiplicativeOperator = ''

        self.sumSoFar = 0.0
        self.factorSoFar = 0.0
        self.waitingForOperand = True

        self.display = QLineEdit('0')
        self.display.setReadOnly(True)
        self.display.setAlignment(Qt.AlignRight)
        self.display.setMaxLength(15)

        font = self.display.font()
        font.setPointSize(font.pointSize() + 10)
        self.display.setFont(font)

        self.digitButtons = []

        for i in range(10):
            self.digitButtons.append(self.createButton(str(i), self.digitClicked))

        self.pointButton = self.createButton(".", self.pointClicked)
        self.clearButton = self.createButton("Clear", self.clear)
        self.clearAllButton = self.createButton("Clear All", self.clearAll)
        self.divisionButton = self.createButton(u"\N{DIVISION SIGN}",self.multiplicativeOperatorClicked)
        self.timesButton = self.createButton(u"\N{MULTIPLICATION SIGN}", self.multiplicativeOperatorClicked)
        self.minusButton = self.createButton("-", self.additiveOperatorClicked)
        self.plusButton = self.createButton("+", self.additiveOperatorClicked)
        self.equalButton = self.createButton("=", self.equalClicked)

        mainLayout = QGridLayout()
        mainLayout.setSizeConstraint(QLayout.SetFixedSize)

        mainLayout.addWidget(self.display, 0, 1, 1, 4)

        mainLayout.addWidget(self.clearButton, 1, 1, 1, 2)
        mainLayout.addWidget(self.clearAllButton, 1, 3, 1, 2)

        mainLayout.addWidget(self.digitButtons[1], 4, 1)
        mainLayout.addWidget(self.digitButtons[2], 4, 2)
        mainLayout.addWidget(self.digitButtons[3], 4, 3)
        mainLayout.addWidget(self.digitButtons[4], 3, 1)
        mainLayout.addWidget(self.digitButtons[5], 3, 2)
        mainLayout.addWidget(self.digitButtons[6], 3, 3)
        mainLayout.addWidget(self.digitButtons[7], 2, 1)
        mainLayout.addWidget(self.digitButtons[8], 2, 2)
        mainLayout.addWidget(self.digitButtons[9], 2, 3)

        mainLayout.addWidget(self.digitButtons[0], 5, 1)
        mainLayout.addWidget(self.pointButton, 5, 2)
        mainLayout.addWidget(self.equalButton, 5, 3)

        mainLayout.addWidget(self.divisionButton, 2, 4)
        mainLayout.addWidget(self.timesButton, 3, 4)
        mainLayout.addWidget(self.minusButton, 4, 4)
        mainLayout.addWidget(self.plusButton, 5, 4)

        self.setLayout(mainLayout)

        self.setWindowIcon(QIcon('logo.png'))
        self.setWindowTitle("Калькулятор")

    def digitClicked(self):
        clickedButton = self.sender()
        digitValue = int(clickedButton.text())

        if self.display.text() == '0' and digitValue == 0.0:
            return

        if self.waitingForOperand:
            self.display.clear()
            self.waitingForOperand = False

        self.display.setText(self.display.text() + str(digitValue))

    def additiveOperatorClicked(self):
        clickedButton = self.sender()
        clickedOperator = clickedButton.text()
        operand = float(self.display.text())

        if self.MultiplicativeOperator:
            if not self.calculate(operand, self.MultiplicativeOperator):
                self.abortOperation()
                return

            self.display.setText(str(self.factorSoFar))
            operand = self.factorSoFar
            self.factorSoFar = 0.0
            self.MultiplicativeOperator = ''

        if self.AdditiveOperator:
            if not self.calculate(operand, self.AdditiveOperator):
                self.abortOperation()
                return

            self.display.setText(str(self.sumSoFar))
        else:
            self.sumSoFar = operand

        self.AdditiveOperator = clickedOperator
        self.waitingForOperand = True

    def multiplicativeOperatorClicked(self):
        clickedButton = self.sender()
        clickedOperator = clickedButton.text()
        operand = float(self.display.text())

        if self.MultiplicativeOperator:
            if not self.calculate(operand, self.MultiplicativeOperator):
                self.abortOperation()
                return

            self.display.setText(str(self.factorSoFar))
        else:
            self.factorSoFar = operand

        self.MultiplicativeOperator = clickedOperator
        self.waitingForOperand = True

    def equalClicked(self):
        operand = float(self.display.text())

        if self.MultiplicativeOperator:
            if not self.calculate(operand, self.MultiplicativeOperator):
                self.abortOperation()
                return

            operand = self.factorSoFar
            self.factorSoFar = 0.0
            self.MultiplicativeOperator = ''

        if self.AdditiveOperator:
            if not self.calculate(operand, self.AdditiveOperator):
                self.abortOperation()
                return

            self.AdditiveOperator = ''
        else:
            self.sumSoFar = operand

        self.display.setText(str(self.sumSoFar))
        self.sumSoFar = 0.0
        self.waitingForOperand = True

    def pointClicked(self):
        if self.waitingForOperand:
            self.display.setText('0')

        if "." not in self.display.text():
            self.display.setText(self.display.text() + ".")

        self.waitingForOperand = False

    def clear(self):
        if self.waitingForOperand:
            return

        self.display.setText('0')
        self.waitingForOperand = True

    def clearAll(self):
        self.sumSoFar = 0.0
        self.factorSoFar = 0.0
        self.AdditiveOperator = ''
        self.MultiplicativeOperator = ''
        self.display.setText('0')
        self.waitingForOperand = True

    def createButton(self, text, member):
        button = Button(text)
        button.clicked.connect(member)
        return button

    def abortOperation(self):
        self.clearAll()
        self.display.setText("Error")

    def calculate(self, rightOperand, Operator):
        if Operator == "+":
            self.sumSoFar += rightOperand
        elif Operator == "-":
            self.sumSoFar -= rightOperand
        elif Operator == u"\N{MULTIPLICATION SIGN}":
            self.factorSoFar *= rightOperand
        elif Operator == u"\N{DIVISION SIGN}":
            if rightOperand == 0.0:
                return False

            self.factorSoFar /= rightOperand

        return True


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    calc = Calculator()
    calc.show()
    sys.exit(app.exec_())
