def hello(name="Roland"):
    return print('Привет ', name)


def main():
    hello()
    hello('Alex')


main()
