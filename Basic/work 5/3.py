# Задание 3 Создайте программу-калькулятор,
# которая поддерживает четыре операции:
# сложение, вычитание, умножение, деление.
# Все данные должны вводиться в цикле, пока пользователь не укажет,
# что хочет завершить выполнение программы.
# Каждая операция должна быть реализована в виде отдельной функции.
# Функция деления должна проверять данные на корректность
# и выдавать сообщение об ошибке в случае попытки деления на ноль.


def add(first, second):
    return first + second


def sub(first, second):
    return first - second


def div(first, second):
    if second != 0:
        result = first / second
    else:
        result = "error, second number can't be ", second
    return result


def mul(first, second):
    return first * second


def calc():
    while True:
        print('Меню')
        print('1. Калькулятор ')
        print('2. Закрыть')
        menu = input('Выюрать: ')
        if menu == '1':
            flag = 1
            while flag:
                first = float(input('Первое число: '))
                print('Выбери действие')
                print('1. Додать ')
                print('2. Отнять')
                print('3. Делить')
                print('4. Умножить')
                operation = input(">>> ")
                second = float(input('Второе число: '))
                result = None
                flag = 0
                if operation == '1':
                    result = add(first, second)
                elif operation == '2':
                    result = sub(first, second)
                elif operation == '3':
                    result = div(first, second)
                elif operation == '4':
                    result = mul(first, second)
                else:
                    print(' не готово')
                print(result)
                continue
        elif menu == '2':
            break


def main():
    calc()


main()