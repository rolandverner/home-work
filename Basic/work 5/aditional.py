# Задание Создайте программу, которая состоит из функции,
# которая принимает три числа и возвращает их среднее арифметическое,
# и главного цикла, спрашивающего у пользователя числа и вычисляющего
# их средние значения при помощи созданной функции.


def func(first, second, third):
    return sum([first, second, third]) / 3


def main():
    first = float(input('Первое число: '))
    second = float(input('Второе число: '))
    third = float(input('Третье число: '))

    print(func(first, second, third))


main()
