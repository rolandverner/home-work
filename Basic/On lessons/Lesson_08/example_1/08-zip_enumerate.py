# Встроенная функция zip возвращает объект-итератор, возвращающий
# кортежи, состоящие из соответствующих элементов заданных
# последовательностей. Количество элементов, которые возвращает
# итератор, равно длине наименьшей из последовательностей.

nodes = ['node1', 'node2', 'node3']
weights = [1, 7, 5, 5, 9, 3]
a = [True , False]

for node, weight , i in zip(nodes, weights , a):
    print(node  , weight , i)
    # print('The weight of node', node, 'is', weight)

# Встроенная функция enumerate возвращает объект-итератор,
# возвращающий пары индексов и значений последовательности
# То есть, поведение enumerate(seq) аналогично
# zip(range(seq), seq))

# for i, x in enumerate(nodes):
#     print(i)
#     print(x)
#     # print('nodes[{}] = {}'.format(i, x))
#
#
# for i in nodes:
#     print(i)