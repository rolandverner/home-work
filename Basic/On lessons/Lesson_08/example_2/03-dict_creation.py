﻿"""Создание словарей"""

# Все эти примеры создают одинаковые словари
a = dict(one=1, two=2, three=3)
b = {
    'one': 1,
    'two': 2,
    'three': 3
}
c = dict(zip(['one', 'two', 'three'], [1, 2, 3]))
d = dict([
    ('two', 2),
    ('one', 1),
    ('three', 3)
])
e = dict(
    {'three': 3,
     'one': 1,
     'two': 2
     })

print(a == b == c == d == e)
print(5 <= 10 <= 25)

print(a)
print(b)
print(c)
print(d)
print(e)

print()

# Использование включений словарей (аналогично списковым включениям)
print({string: string.upper() for string in ('one', 'two', 'three')})