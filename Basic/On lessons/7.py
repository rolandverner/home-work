# try:
#     a = float(input('a: '))
#     b = float(input('b: '))
# except ValueError as e:
#     print('Вводи числа!')
# except Exception as e:
#     print(type(e))

#
# def calculator():
#     loop = True
#     while loop:
#         try:
#             a = float(input("a: "))
#             b = float(input("b: "))
#             print(a / b)
#             loop = False
#         except (ValueError, ZeroDivisionError) as error:
#             print("Error", error)
#
# if __name__ == '__main__':
#     calculator()

def add(first, second):
    return first + second


def sub(first, second):
    return first - second


def div(first, second):
    return first / second


def mul(first, second):
    return first * second


MATH_OPERATION = {
    '+': add,
    '-': sub,
    '/': div,
    '*': mul
}
def vvod():
    a = float(input("a: "))
    b = float(input("b: "))
    oper = input("Operator: ")
    return {
        'first': a,
        'oper': oper,
        'second': b
    }
if __name__ == '__main__':
    input_user = vvod()
    print(input_user)
    res = None
    res = MATH_OPERATION[input_user['oper']](input_user['first'], input_user['second'])
    if res:
        print(res)