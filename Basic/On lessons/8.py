# nodes = ['node1','node2','node3','node4']
#
# for x,y in enumerate(nodes):
#     print(x, y)

# a, b, *rest = range(10)
# print(a)
# print(b)
# print(rest)

# def multiply(*numbers):
#     result = 1
#     for x in numbers:
#         result *= x
#         print(result)
#     return
#
#
# multiply(2, 3)

def getint():
    for i in range(10):
        yield i
        yield i+1

print(list(getint()))