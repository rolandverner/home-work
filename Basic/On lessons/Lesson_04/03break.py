# name = None  # вначале мы не знаем имени пользователя
#
# # бесконечный цикл
# while True:
#     print('Меню:')
#     print('1. Ввести имя')
#     print('2. Вывести приветствие')
#     print('3. Выйти')
#     response = input('Выберите пункт: ')
#
#     print()
#
#     if response == '1':
#         name = input('Введите ваше имя: ')
#
#     elif response == '2':
#         if name is not None:
#             print('Привет, ', name, '!')
#             break
#         else:
#             print('Я не знаю вашего имени.')
#     elif response == '3':
#         print('Вы выбрали "ВЫЙТИ! Выходите ."')
#         break
#     else:
#         print('Неверный ввод.')
#
#     print()
#
#

MY_NAME = 'Anton'
flag = True
while flag:
    your_name = input('Please enter your name >>> ')
    if your_name == MY_NAME:
        print('Cool!')
        flag = False
    else:
        print('Not cool!')