for i in range(10):
    print('i =', i)

my_range = range(10)

iter_custom = iter(my_range)
while True:
    try:
        print(next(iter_custom))
    except StopIteration:
        break



