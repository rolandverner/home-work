﻿def function():
    # global указывает, что необходимо получать доступ к глобальной переменной
    # var, а не создавать новую локальную при попытке что-либо ей присвоить
    global var
    # вывод значения глобальной переменной на экран
    print(var)
    # изменение глобальной переменной
    var = 'новое значение'
    # вывод значения глобальной переменной на экран
    print(var)

var = 'глобальная переменная'
print(var)
function()
print(var)


odd_list  = [1,3,5,7,9]
even_list = [2,4,6,8,10]
obj_lists ={
    'odd' : odd_list,
    'even' : even_list
}

print(obj_lists['odd'])



some_list = [1,2,3,4,5,6]


def fun(a):
    for elem in a:
        print(elem)

def add(a,b):
    return a + b

def sub(a,b):
    return a - b

def div(a,b):
    if b == 0:
        print('на ноль делить нельзя')
        b = float(input(' >>> '))
        return  a / b
    else:
        return a / b

def mul(a,b):
    return a * b

ready = True
while ready:
    first_num  = float(input('>>> '))
    operation = input(' + - * / ')
    second_num  = float(input('>>> '))
    res  = None
    if operation == '+':
        res = add(first_num , second_num)
    elif operation == '-':
        res = sub(first_num , second_num)
    elif operation == '/':
        res = div(first_num , second_num)
    elif operation == '*':
        res = mul(first_num , second_num)
    else:
        print('Нет такой операции в нашем калькуляторе(((')

    if res is not None:
        print('{} {} {} = {}'.format(first_num , operation , second_num , res))
    else:
        print('result is NONE!!!!')

    print('Хотите посчитать еще ? да/нет , д/н  , yes/no , y/n ')
    response  = input(" >>> ").replace(' ' , '').lower()
    if response == 'нет' or response == 'н' or response == 'no' or response == 'n':
        print('Спасиб что считали с нами :) ')
        ready = False
    elif response == 'да' or response == 'д' or response == 'yes' or response == 'y':
        ready = True
    else:
        print('Пожалуйста, введите что то нормальное ))')









