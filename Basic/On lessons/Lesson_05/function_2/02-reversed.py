# reversed позволяет обходить последовательность в обратном порядке
for i in reversed(range(5)): # range(5)  = [0 ,1,2,3,4] => reverserd([0 ,1,2,3,4]) = [4,3,2,1,0]
    print(i)