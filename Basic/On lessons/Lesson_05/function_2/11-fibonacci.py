# Числа Фибоначчи — последовательность, в которой первые два числа равны единице,
# а все последующие — сумме двух предыдущих. 1 1 2 3 5 8 13 21 34 55


def fib(n):
    if n == 1 or n == 2:  # условие выхода
        return 1
    else:
        return fib(n - 1) + fib(n - 2)  # рекурсивный вызов fib(3) = > fib(2) + fib(1) , fib(4) => fib(3) (fib(2) + fib(1)) + fib(2)


index = int(input('Введите номер числа Фибоначчи: '))
print(fib(index))