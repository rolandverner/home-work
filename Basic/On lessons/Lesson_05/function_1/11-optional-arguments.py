# Если параметр name не задан, то name = 'Alex'
def hello(name='user'):
    """Строка, стоящая в самом начале функции (а также модуля, класса или метода),
     играет роль особого вида комментариев – документационной строки (docstring).
     """
    print('Hello, ', name, '!', sep='')


def test(a , b):
    """
    :param a: first-par
    :param b: second-par
    :return:  first-par + second-par
    """
    return a + b

x = test(2 , 2 )
print(test.__doc__)
print(x)
hello('Python')
hello('Anton')
hello('Nastya')
hello('Sasha')
hello()
print(hello.__doc__)


