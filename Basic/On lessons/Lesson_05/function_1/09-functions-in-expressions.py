# def add(a, b):
#     return a + b
#
# def sub(a, b):
#     return a - b
#
#
# # Вызов функции может быть частью выражения
# res = add(2, 3) + sub(2, 3)
#
# print(res)  # => print((2 + 3) + (2 - 3))

def add(a, b):
    return a + b

def sub(a, b):
    return a - b

def div(a , b):
    return a / b

def mul(a , b):
    return a * b


def check_operation(x, operation, y):
    res = None
    if operation == '+':
        res = add(x, y)
    elif operation == '-':
        res = sub(x, y)
    elif operation == '*':
        res = mul(x, y)
    elif operation == '/':
        res = div(x, y)
    else:
        res = 'Error'
        return res
    return res


def main():
    first = float(input(">>> "))
    oper = input(">>>  + -  *  /")
    second = float(input(">>> "))
    result_of_function  = check_operation(first , oper , second)
    if result_of_function != "Error":
        print(result_of_function)

main()
