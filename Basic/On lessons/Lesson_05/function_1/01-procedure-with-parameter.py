def print_numbers(test):
    for i in range(test):
        if i == 3:
            break
        print(i)

# print_numbers(10)

range_input = int(input('>>> '))

print_numbers(range_input)

# print_numbers(test=5)
# print_numbers(32)
# print_numbers(3)
# print_numbers(3)
# print_numbers(3)
# print_numbers(3)
# print_numbers(3)
# print_numbers(3)
# print_numbers(2343)
# print_numbers(233)

