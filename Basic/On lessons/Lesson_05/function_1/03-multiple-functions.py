def print_numbers(limit):
    for i in range(limit):
        print(i)
    print("function is completed!")

# Любое логически завершённое действие следует помещать в функцию

def main():
    n = input('Введите n: ')
    a = int(n)
    n  = a
    print_numbers(n)

# Вызов главной функции

main()



