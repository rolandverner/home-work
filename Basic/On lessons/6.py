# def outer_function(a=1, b=1):
#     def inner_function():
#         return a ** 2 + b ** 2
#
#     return round(inner_function() ** 0.5, 2)
#
#
# print(outer_function(5, 10))
#
#
# class Pifagor:
#
#     def __init__(self, a=1, b=1):
#         self.a = a
#         self.b = b
#
#     def calc(self):
#         return round((self.a ** 2 + self.b ** 2) ** 0.5, 2)
#
#
# Pif = Pifagor(3, 4)
# print(Pif.calc())
# print(u"\N{PLUS-MINUS SIGN}")

n = 1000000
k = 1

for i in range(n):
    k = k * (i+1)
    print(k)
