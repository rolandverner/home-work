# Список
my_list = [1, 2, 3]

# Итерирование
print('Iterating:')
for element in my_list:
    print(element)

print()

# Получение доступа к элементам при помощи целочисленных ключей (индексация)
print('Indexing:')
print(my_list[0])
print(my_list[2])
print(my_list[-1])

print()

# Длина последовательности
print('Length:', len(my_list))
