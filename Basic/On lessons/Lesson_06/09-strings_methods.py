some_str  = '         Lorem ipsum dolor sit amet     '
test = 'sadasdasd'

print(some_str)

print(some_str.find('L')) # find - ищет в строке some_str подстроку "L" , возвращает индекс  , иначе  -1

print(some_str.rfind('o')) # Поиск подстроки в строке. Возвращает номер последнего вхождения или -1
print(some_str.lower())
print(some_str.upper())
print(some_str.index('o')) # Поиск подстроки в строке. Возвращает номер первого вхождения или вызывает ValueError

print(some_str.rindex('o')) # Поиск подстроки в строке. Возвращает номер последнего вхождения или вызывает ValueError


print(some_str.replace('e' , '--- ---'))
print(some_str.split(' ')) #	Разбиение строки по разделителю
print(some_str.isdigit()) #	Состоит ли строка из цифр
print(test.isalpha()) #	Состоит ли строка из букв

print(some_str.startswith('Lo'))
print(some_str.endswith('et'))

print(some_str.lstrip()) #Удаление пробельных символов в начале строки
print(some_str.rstrip()) #Удаление пробельных символов в конце строки
print(some_str.strip())