"""
Последовательности одинаковых типов можно сравнивать. Сравнения происходят
в лексикографическом порядке: последовательность меньшей длины меньше, чем
последовательность большей длины, если же их длины равны, то результат
сравнения равен результату сравнения первых отличающихся элементов.
"""

print('abc' >'cab')
print('ABCD' < 'abcd')

words = ['lorem', 'ipsum', 'dolor', 'sit', 'amet']
print(sorted(words))

a = [1,2,3]
print(a.pop(0))