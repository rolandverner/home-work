# примеры комплексных чисел
c1 = 2 + 3j  # 2 + 3i, 2 - действительная часть, 3 - мнимая
c2 = 5 - 5j  # 5 - 5i
c6= 5 + 1j

print(type(c6))
# построение комплексного числа из вещественных
# a = 2
# b = 3
# c3 = complex(a, b)
# c4 = complex(5, -5)

print(c1,c2,c6)
# print(c3, c4)
# print(type(c1))
