# числа в десятичной системе счисления
# dec1 = 8
# dec2 = 42
# dec3 = -3
# dec4 = 25802836572356723058203845293402834028304820938402834023580235777082489436236
#
# print(type(dec1))
# print(dec2)
# print(dec3)
# print(type(dec4))
# print()

"""
    block comment
"""
# # # числа в шестнадцатеричной системе счисления
# hex1 = 0x9
# hex2 = 0xA
# hex3 = 0xFF
# hex4 = 0x3de
# hex5 = 0x202
# #
# print(hex1)
# print(hex2)
# print(hex3)
# print(hex4)
# print(hex5)
# print()
# #
# # число в двоичной системе счисления
# bin1 = 0b11101101
# print(bin1)
# print()
#
# # # число в восьмеричной системе счисления
oct1 = 0o765
print(oct1)
# #
# #
# # # построение целого числа из другого значения
x = "15"
print(x)
print(type(x))
number = int(x)
print(type(number))
print(number)
print(number + 5)
print(x + 5 ) #-- ошибка
#
