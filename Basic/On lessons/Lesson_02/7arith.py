# # import math  # импротируем модуль math
# #
# # from tkinter import *
# #
# # # PyQt5  eval()
# # root = Tk()
# # root.title("Hello World!")
# # root.geometry('300x40')
# #
# # def button_clicked():
# #     print("Hello World!")
# #
# # def close():
# #     root.destroy()
# #     root.quit()
# #
# # button = Button(root, text="Press Me", command=button_clicked)
# # button.pack(fill=BOTH)
# #
# # root.protocol('WM_DELETE_WINDOW', close)
# #
# # root.mainloop()
#
#
# PI = math.pi
# NUMBER_E = math.e
#
# print(PI)  # константа пи
# print(NUMBER_E)   # число Эйлера
#
# CONST  = 5
# print(CONST)
# CONST = 10
# print(CONST)
#
# x = -3.265
# # целое число, ближайшее целое снизу, ближайшее целое сверху
# print(math.trunc(x), math.floor(x), math.ceil(x))
#
#
#
#
# y = math.sin(PI / 4)  # math.sin – синус
# print(y)
# #
# y = 1 / math.sqrt(2)  # math.sqrt – квадратный корень
# print(round(y, 2))
#

x = 1
y = 15

operation  = input(" >>> ")
if operation == "+":
    print(x + y)
elif operation == '-':
    print(x - y)