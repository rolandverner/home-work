class Person():
    def __init__(self, name='username', surname=None, age=None):
        self.name = name
        self.surname = surname
        self.age = age

    def print_dat(self):
        print('name', self.name, 'Surname', self.surname, 'Age', self.age)


roland = Person('Roland', 'Verner', 23)
test = Person()

roland.print_dat()
test.print_dat()