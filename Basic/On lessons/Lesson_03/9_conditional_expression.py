is_ready = False

# То же самое, что и в предыдущем примере, но используем
# условное выражение вместо условного оператора









if is_ready:
    state_msg1 = 'ready'
else:
    state_msg1 = 'not ready yet!'
print(state_msg1)



state_msg = 'Ready' if is_ready else 'Not ready yet'
print(state_msg)