﻿import math

str1 = 'hel'
str2 = 'lo'
result = str1 + str2
print(result)


# msg  = f('str' , str1)

# форматирование строк

a = 48
b = 73

message1 = '%d + %d = %d' % (a, b, a + b)
print(message1)

t = '{1} - {2} = {0}'
message2 = t.format(a, b, a - b)
print(message2)


# индексация строк

s = 'hello ,World!'
#
# # (вернуться в седьмом уроке)
# print(s[0])   # индексация начинается с нуля
# print(s[4])   # четвёртый (пятый логически) элемент (символ)
# print(s[-1])  # отрицательные числа – индексация с конца
#
# print(s[2:7])    # символы со второго (включительно) по пятый (не включительно)
# print(s[2:7:2])  # то же, но с шагом два
print(s.replace(' ', '@'))
# Разбиение строки по разделителю
print('split')
print(s.split(" "))
# Состоит ли строка из цифр
print('isdigit')
print(s.isdigit())
print('-------------------')
# Состоит ли строка из букв
print('isalpha')
print(s.isalpha())
# Состоит ли строка из цифр или букв
print('isalnum')
print(s.isalnum())
# Состоит ли строка из символов в нижнем регистре
print('islower')
print(s.islower())
# Состоит ли строка из символов в верхнем регистре
print('isupper')
print(s.isupper())
print('--------')

# Начинаются ли слова в строке с заглавной буквы
print('istitle')
print(s.istitle())
# Преобразование строки к верхнему регистру
print('upper')
print(s.upper())
# Преобразование строки к нижнему регистру
print('lower')
print(s.lower())
# Начинается ли строка s с шаблона str
print('startswith')
print(s.startswith('H'))
# Заканчивается ли строка S шаблоном str
print('endswith')
print(s.endswith('w'))

# Символ в его код ASCII
print('ord')
print(ord('a'))
# Код ASCII в символ
print('chr')
print(chr(65))
# Переводит первый символ строки в верхний регистр, а все остальные в нижний
print('capitalize')
print(s.capitalize())
# Возвращает отцентрованную строку, по краям которой стоит символ fill (пробел по умолчанию)
s23 = s.center(14, '*' )
print(s23)
print(len(s23))