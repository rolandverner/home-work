is_ready = True

# Присваиваем значение переменной в зависимости от условия
if is_ready:
    state_msg = 'Ready'
    is_ready = True
else:
    state_msg = 'Not ready yet'


if is_ready:
    tmp = 'is ready is true'
else:
    tmp = 'is_ready is false'


print(state_msg)
print(tmp)