# 6) Написать программу:
# Присвоить переменной number любое число от 0 до 5
# присвоить переменной text значение "нет результата"
# Используя условные операторы if - elif осуществить проверку:
# - если number равен 0 вывести на печать 0
# - если number равен 1 вывести на печать 1
# - если number равен 2 вывести на печать 2
# - если number равен 3 вывести на печать 3
# - если number равен 4 вывести на печать 4
# - если number равен 5 вывести на печать 5
# - если number равен 6 (или любому другому числу вне диапазона 0 - 5) вывести на печать "Ошибка" и "нет результата"
import random

number = random.randint(0, 6)
print(number)
text = "нет результата"
if number == 0:
    print(number)
elif number == 1:
    print(number)
elif number == 2:
    print(number)
elif number == 3:
    print(number)
elif number == 4:
    print(number)
elif number == 5:
    print(number)
else:
    print("Ошибка", text)