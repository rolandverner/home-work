import math

a = float(input("a = "))
b = float(input("b = "))
c = float(input("c = "))

D = b ** 2 - 4 * a * c
print("Дискриминант", round(D, 2))
if D > 0:
    x1 = (-b + math.sqrt(D)) / (2 * a)
    x2 = (-b - math.sqrt(D)) / (2 * a)
    print("x1 = {}, x2 = {}".format(round(x1, 2), round(x2, 2)))
elif D == 0:
    x = -b / (2 * a)
    print("x = ", round(x, 2))
else:
    print("Корней нет")