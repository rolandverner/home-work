from math import (cos, pi)

PI = pi
x = float(input('Укажите аргумент '))
if -PI <= x <= PI:
    y = cos(3 * x)
    print('Y равен : ', y)
    print('Х равен : ', x)
elif x < -PI or x > PI:
    y = x
    print('Y равен :', y, 'Х равен :', x)
else:
    print('Указаный аргумент не пренадлежит функции')
