# 4) Запросить у пользователя два числа.
# - Если первое больше второго, то вычислить их разницу и вывести данные на печать.
# - Если второе число больше первого, то вычислить их сумму и вывести на печать
# - Если оба числа равны, то вывести это значение на печать.
# - Постарайтесь использовать только три переменные

number_one = float(input('Введите 1 число: '))
number_two = float(input('Введите 2 число: '))

if number_one > number_two:
    result = number_one - number_two
    print('Разница: ', result)

elif number_one < number_two:
    result = number_one + number_two
    print('Сумма: ', result)

elif number_one == number_two:
    print('Числа одинаковы: ', number_one)